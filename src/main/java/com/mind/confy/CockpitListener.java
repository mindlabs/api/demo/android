package com.mind.confy;

public interface CockpitListener {

    void onCockpitCameraButtonClicked();
    void onCockpitMicrophoneButtonClicked();
    void onCockpitSpeakerButtonClicked();
    void onCockpitExitButtonClicked();
    void onCockpitExpandCollapseButtonClicked();

}
