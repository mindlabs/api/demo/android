package com.mind.confy;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.mind.confy.databinding.CockpitBinding;

import java.util.HashMap;
import java.util.Map;

public class Cockpit extends ConstraintLayout {

    public static final int CAMERA_ON =  R.drawable.camera_on;
    public static final int CAMERA_OFF = R.drawable.camera_off;

    public static final int MICROPHONE_ON =  R.drawable.microphone_on;
    public static final int MICROPHONE_OFF = R.drawable.microphone_off;

    public static final int SPEAKER_ON =  R.drawable.speaker_on;
    public static final int SPEAKER_OFF = R.drawable.speaker_off;

    public static final int EXPANDED =  R.drawable.expanded;
    public static final int COLLAPSED = R.drawable.collapsed;

    private CockpitBinding binding;
    private CockpitListener listener;

    public Cockpit(Context context) {
        super(context);
        init(context);
    }

    public Cockpit(Context context, AttributeSet attributes) {
        super(context, attributes);
        init(context);
    }

    public void setListener(CockpitListener listener) {
        this.listener = listener;
    }

    public void setRoomURI(String roomURI) {
        binding.qrCode.setImageBitmap(encodeTextIntoQRCodeBitmap(roomURI));
    }

    public void setCameraButtonEnabled(boolean enabled) {
        binding.cameraButton.setEnabled(enabled);
    }

    public void setCameraButtonBackground(int resource) {
        binding.cameraButton.setBackgroundResource(resource);
    }

    public void setMicrophoneButtonEnabled(boolean enabled) {
        binding.microphoneButton.setEnabled(enabled);
    }

    public void setMicrophoneButtonBackground(int resource) {
        binding.microphoneButton.setBackgroundResource(resource);
    }

    public void setSpeakerButtonBackground(int resource) {
        binding.speakerButton.setBackgroundResource(resource);
    }

    public void setExpandCollapseButtonBackground(int resource) {
        binding.expandCollapseButton.setBackgroundResource(resource);
    }

    private void init(Context context) {
        binding = CockpitBinding.inflate(LayoutInflater.from(context), this, true);
        binding.microphoneButton.setOnClickListener(view -> {
            if (listener != null) {
                listener.onCockpitMicrophoneButtonClicked();
            }
        });
        binding.cameraButton.setOnClickListener(view -> {
            if (listener != null) {
                listener.onCockpitCameraButtonClicked();
            }
        });
        binding.speakerButton.setOnClickListener(view -> {
            if (listener != null) {
                listener.onCockpitSpeakerButtonClicked();
            }
        });
        binding.exitButton.setOnClickListener(view -> {
            if (listener != null) {
                listener.onCockpitExitButtonClicked();
            }
        });
        binding.expandCollapseButton.setOnClickListener(view -> {
            if (listener != null) {
                listener.onCockpitExpandCollapseButtonClicked();
            }
        });
    }

    private Bitmap encodeTextIntoQRCodeBitmap(String text) {
        QRCodeWriter writer = new QRCodeWriter();
        try {
            Map<EncodeHintType, Object> hints = new HashMap<>();
            hints.put(EncodeHintType.MARGIN, 0);
            BitMatrix bitMatrix = writer.encode(text, BarcodeFormat.QR_CODE, 220, 220, hints);
            int w = bitMatrix.getWidth();
            int h = bitMatrix.getHeight();
            Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            for (int x = 0; x < w; x++) {
                for (int y = 0; y < h; y++) {
                    bitmap.setPixel(x, y, bitMatrix.get(x, y) ? Color.WHITE : Color.TRANSPARENT);
                }
            }
            return bitmap;

        } catch (WriterException exception) {
            throw new RuntimeException(exception);
        }
    }

}