package com.mind.confy;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.UiThread;
import com.mind.api.sdk.BuildConfig;
import com.mind.api.sdk.ConferenceLayout;
import com.mind.api.sdk.ParticipantRole;
import java9.util.concurrent.CompletableFuture;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;

public class MindAPI {

    private static final String APPLICATION_URI = "https://api.mind.com/00e1db72-c57d-478c-88be-3533d43c8b34";
    private static final String APPLICATION_TOKEN = "W1VJggwnvg2ldDdvSYES07tpLCWLDlD5nFakVJ6QSCPiZRpAMGyAzKW07OpM1IpceZ2WT5h5Mu7Ekt7WDTQzMUoQkVTRE4NdYUFE";

    private static final int HTTP_CONNECTION_TIMEOUT_SECONDS = 10;
    private static final ExecutorService HTTP_EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();
    private static final Handler MAIN_LOOPER_HANDLER = new Handler(Looper.getMainLooper());

    @UiThread
    static CompletableFuture<String> createConference(String name, ConferenceLayout layout) {
        try {
            return newHttpPost(APPLICATION_URI, new JSONObject().put("name", name).put("layout", layout).put("features", new JSONArray(new String[]{}))).thenApply(responseDTO -> {
                try {
                    int responseStatus = responseDTO.getInt("status");
                    if (responseStatus == 200) {
                        JSONObject conferenceDTO = responseDTO.getJSONObject("data");
                        return APPLICATION_URI + "/" + conferenceDTO.getString("id");
                    } else {
                        throw new RuntimeException("Can't create a room: " + responseStatus);
                    }
                } catch (JSONException exception) {
                    throw new RuntimeException(exception);
                }
            });
        } catch (JSONException exception) {
            throw new RuntimeException(exception);
        }
    }

    @UiThread
    static CompletableFuture<String> createParticipant(String conferenceURI, String name, ConferenceLayout layout, ParticipantRole role) {
        try {
            return newHttpPost(conferenceURI + "/participants", new JSONObject().put("name", name).put("layout", layout).put("role", role)).thenCompose(postResponseDTO -> {
                try {
                    int postResponseStatus = postResponseDTO.getInt("status");
                    switch (postResponseStatus) {
                        case 200: {
                            JSONObject participantDTO = postResponseDTO.getJSONObject("data");
                            return CompletableFuture.completedFuture(participantDTO.getString("token"));
                        }
                        case 403:
                            return newHttpGet(conferenceURI + "/participants").thenApply(responseDTO -> {
                                try {
                                    int responseStatus = responseDTO.getInt("status");
                                    if (responseStatus == 200) {
                                        JSONArray participantDTOs = responseDTO.getJSONArray("data");
                                        for (int i = 0; i < participantDTOs.length(); i++) {
                                            JSONObject participantDTO = participantDTOs.getJSONObject(i);
                                            if (!participantDTO.getBoolean("online")) {
                                                return participantDTO;
                                            }
                                        }
                                        return null;
                                    } else {
                                        throw new RuntimeException("Can't get the list of participants: " + responseStatus);
                                    }
                                } catch (JSONException exception) {
                                    throw new RuntimeException(exception);
                                }
                            }).thenCompose((offlineParticipant) -> {
                                try {
                                    if (offlineParticipant != null) {
                                        return newHttpDelete(conferenceURI + "/participants/" + offlineParticipant.getString("id")).thenCompose((responseDTO) -> {
                                            try {
                                                int responseStatus = responseDTO.getInt("status");
                                                if (responseStatus == 204) {
                                                    return MindAPI.createParticipant(conferenceURI, name, layout, role);
                                                } else {
                                                    throw new RuntimeException("Can't free space in the room: " + responseStatus);
                                                }
                                            } catch (JSONException exception) {
                                                throw new RuntimeException(exception);
                                            }
                                        });
                                    } else {
                                        throw new RuntimeException("The room is full");
                                    }
                                } catch (JSONException exception) {
                                    throw new RuntimeException(exception);
                                }
                            });
                        case 404:
                            throw new RuntimeException("The room doesn't exist");
                        default:
                            throw new RuntimeException("Can't create a new participant: " + postResponseStatus);
                    }
                } catch (JSONException exception) {
                    throw new RuntimeException(exception);
                }
            });
        } catch (JSONException exception) {
            throw new RuntimeException(exception);
        }
    }

    private static CompletableFuture<JSONObject> newHttpPost(String uri, JSONObject dto) {
        return newHttpRequest("POST", uri, dto);
    }

    private static CompletableFuture<JSONObject> newHttpGet(String uri) {
        return newHttpRequest("GET", uri, null);
    }

    private static CompletableFuture<JSONObject> newHttpDelete(String uri) {
        return newHttpRequest("DELETE", uri, null);
    }

    private static CompletableFuture<JSONObject> newHttpRequest(String method, String url, JSONObject dto) {
        final CompletableFuture<JSONObject> result = new CompletableFuture<>();
        final AtomicReference<Future<?>> future = new AtomicReference<>();
        future.set(HTTP_EXECUTOR_SERVICE.submit(() -> {
            HttpURLConnection connection = null;
            try {
                connection = (HttpURLConnection) new URL(url).openConnection();
                connection.setRequestMethod(method);
                connection.setConnectTimeout(HTTP_CONNECTION_TIMEOUT_SECONDS * 1000);
                connection.setReadTimeout(HTTP_CONNECTION_TIMEOUT_SECONDS * 1000);
                connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestProperty("Authorization", "Bearer " + APPLICATION_TOKEN);
                connection.setRequestProperty("Mind-SDK", "Mind Android SDK " + BuildConfig.VERSION_NAME);
                connection.setRequestProperty("User-Agent", "Android " + Build.VERSION.SDK_INT);
                if (dto != null) {
                    connection.setDoOutput(true);
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(dto.toString());
                    writer.close();
                }
                int responseCode = connection.getResponseCode();
                JSONObject responseDTO = new JSONObject();
                responseDTO.put("status", responseCode);
                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(200 <= responseCode && responseCode <= 299 ? connection.getInputStream() : connection.getErrorStream(), StandardCharsets.UTF_8));
                for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                    sb.append(line).append("\n");
                }
                reader.close();
                String content = sb.toString().trim();
                if (content.length() > 0) {
                    switch (content.charAt(0)) {
                        case '[':
                            responseDTO.put("data", new JSONArray(content));
                            break;
                        case '{':
                            responseDTO.put("data", new JSONObject(content));
                            break;
                        default:
                            responseDTO.put("data", content);
                            break;
                    }
                }
                MAIN_LOOPER_HANDLER.post(() -> result.complete(responseDTO));
            } catch (Exception exception) {
                MAIN_LOOPER_HANDLER.post(() -> result.completeExceptionally(exception));
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
        }));
        return result;
    }

}
