package com.mind.confy;

import android.content.Context;
import android.transition.TransitionManager;
import android.util.AttributeSet;
import android.util.TypedValue;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

public class RoomLayout extends ConstraintLayout {

    public RoomLayout(Context context, AttributeSet attributes) {
        super(context, attributes);
    }

    public void expandCockpit() {
        Cockpit cockpit = findViewById(R.id.cockpit);
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(this);
        constraintSet.connect(cockpit.getId(), ConstraintSet.TOP, getId(), ConstraintSet.TOP);
        constraintSet.constrainHeight(cockpit.getId(), dpToPixels(297));
        constraintSet.setMargin(cockpit.getId(), ConstraintSet.BOTTOM, 0);
        TransitionManager.beginDelayedTransition(this);
        constraintSet.applyTo(this);
    }

    public void collapseCockpit() {
        Cockpit cockpit = findViewById(R.id.cockpit);
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(this);
        constraintSet.clear(cockpit.getId(), ConstraintSet.TOP);
        constraintSet.constrainHeight(cockpit.getId(), dpToPixels(62));
        constraintSet.setMargin(cockpit.getId(), ConstraintSet.BOTTOM, dpToPixels(50));
        TransitionManager.beginDelayedTransition(this);
        constraintSet.applyTo(this);
    }

    private int dpToPixels(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

}
