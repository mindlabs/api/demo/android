package com.mind.confy;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import androidx.databinding.DataBindingUtil;
import android.media.AudioManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.mind.api.sdk.Camera;
import com.mind.api.sdk.Conference;
import com.mind.api.sdk.ConferenceLayout;
import com.mind.api.sdk.Me;
import com.mind.api.sdk.MediaStream;
import com.mind.api.sdk.Microphone;
import com.mind.api.sdk.MindSDK;
import com.mind.api.sdk.ParticipantRole;
import com.mind.api.sdk.Session;
import com.mind.api.sdk.SessionListener;
import com.mind.confy.databinding.RoomActivityBinding;
import java9.util.concurrent.CompletionException;
import org.webrtc.RendererCommon.ScalingType;

import java.util.Random;

import static com.mind.confy.Cockpit.CAMERA_ON;
import static com.mind.confy.Cockpit.CAMERA_OFF;
import static com.mind.confy.Cockpit.COLLAPSED;
import static com.mind.confy.Cockpit.EXPANDED;
import static com.mind.confy.Cockpit.MICROPHONE_ON;
import static com.mind.confy.Cockpit.MICROPHONE_OFF;
import static com.mind.confy.Cockpit.SPEAKER_ON;
import static com.mind.confy.Cockpit.SPEAKER_OFF;

public class RoomActivity extends FullscreenActivity implements CockpitListener, SessionListener {

    private static final String TAG = "Confy";
    private static final String[] PARTICIPANT_NAMES = { "Zeus", "Hera", "Poseidon", "Ares", "Athena", "Apollo", "Artemis", "Hephaestus", "Aphrodite", "Hermes", "Dionysus", "Hades", "Hypnos", "Nike", "Janus", "Nemesis", "Iris", "Hecate", "Prometheus", "Cronus" };
    private static final int CAMERA_AND_MICROPHONE_PERMISSION_REQUEST = new Random().nextInt(Short.MAX_VALUE); // (facepalm) Android allow us to use only 16 bits for `requestCode`...

    private RoomActivityBinding binding;
    private Camera camera;
    private Microphone microphone;
    private Session session;
    private Conference conference;
    private MediaStream myStream;
    private Me me;

    private boolean cockpitCollapsed = true;
    private boolean cameraOn = true;
    private boolean microphoneOn = true;
    private boolean speakerOn = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.room_activity);
        binding.conferenceVideo.setScalingType(ScalingType.SCALE_ASPECT_FILL);
        camera = MindSDK.getCamera();
        microphone = MindSDK.getMicrophone();
        String roomURI = getIntent().getStringExtra("ROOM_URI");
        binding.cockpit.setListener(this);
        binding.cockpit.setRoomURI(roomURI);
        String conferenceURI = roomURI.replaceFirst("^(https?://[^/]+).*/(#/|\\?)([^/]+)$", "$1/00e1db72-c57d-478c-88be-3533d43c8b34/$3");
        MindAPI.createParticipant(conferenceURI, PARTICIPANT_NAMES[new Random().nextInt(PARTICIPANT_NAMES.length)], ConferenceLayout.MOSAIC, ParticipantRole.MODERATOR ).thenApply(participantToken -> {
            return MindSDK.join2(conferenceURI, participantToken, this).thenAccept(session -> {
                this.session = session;
                conference = session.getConference();
                me = conference.getMe();
                Log.i(TAG, "Joined conference " + conference.getId() + " as participant " + me.getId());
                MediaStream conferenceStream = conference.getMediaStream();
                conferenceStream.setMaxVideoBitrate(624_000); // 480 kbit/s (which corresponds to 640x360@30) + 30%
                binding.conferenceVideo.setMediaStream(conferenceStream);
                myStream = MindSDK.createMediaStream(microphone, camera);
                me.setMediaStream(myStream);
                cameraOn = !cameraOn;
                onCockpitCameraButtonClicked();
                microphoneOn = !microphoneOn;
                onCockpitMicrophoneButtonClicked();
                speakerOn = !speakerOn;
                onCockpitSpeakerButtonClicked();
                cockpitCollapsed = !cockpitCollapsed;
                onCockpitExpandCollapseButtonClicked();
            });
        }).exceptionally(exception -> {
            if (exception instanceof CompletionException) {
                exception = exception.getCause();
            }
            alert(exception.getMessage());
            return null;
        });;
    }

    @Override
    protected void onDestroy() {
        exitConferenceAndCloseMyStream();
        super.onDestroy();
    }

    @Override
    public void onCockpitCameraButtonClicked() {
        if (!cameraOn) {
            binding.cockpit.setCameraButtonEnabled(false);
            camera.acquire().thenRun(() -> {
                cameraOn = true;
                binding.cockpit.setCameraButtonBackground(CAMERA_ON);
                binding.cockpit.setCameraButtonEnabled(true);
            }).exceptionally((exception) -> {
                binding.cockpit.setCameraButtonEnabled(false);
                if (exception.getCause() instanceof SecurityException) {
                    ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.CAMERA }, CAMERA_AND_MICROPHONE_PERMISSION_REQUEST);
                }
                return null;
            });
        } else {
            camera.release();
            cameraOn = false;
            binding.cockpit.setCameraButtonBackground(CAMERA_OFF);
        }
    }

    @Override
    public void onCockpitMicrophoneButtonClicked() {
        if (!microphoneOn) {
            binding.cockpit.setMicrophoneButtonEnabled(false);
            microphone.acquire().thenRun(() -> {
                microphoneOn = true;
                binding.cockpit.setMicrophoneButtonBackground(MICROPHONE_ON);
                binding.cockpit.setMicrophoneButtonEnabled(true);
            }).exceptionally((exception) -> {
                binding.cockpit.setMicrophoneButtonEnabled(true);
                if (exception.getCause() instanceof SecurityException) {
                    ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.RECORD_AUDIO }, CAMERA_AND_MICROPHONE_PERMISSION_REQUEST);
                }
                return null;
            });
        } else {
            microphone.release();
            microphoneOn = false;
            binding.cockpit.setMicrophoneButtonBackground(MICROPHONE_OFF);
        }
    }

    @Override
    public void onCockpitSpeakerButtonClicked() {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (!speakerOn) {
            audioManager.setSpeakerphoneOn(true);
            speakerOn = true;
            binding.cockpit.setSpeakerButtonBackground(SPEAKER_ON);
        } else {
            audioManager.setSpeakerphoneOn(false);
            speakerOn = false;
            binding.cockpit.setSpeakerButtonBackground(SPEAKER_OFF);
        }
    }

    @Override
    public void onCockpitExitButtonClicked() {
        finish();
    }

    @Override
    public void onCockpitExpandCollapseButtonClicked() {
        if (cockpitCollapsed) {
            binding.layout.expandCockpit();
            binding.cockpit.setExpandCollapseButtonBackground(EXPANDED);
        } else {
            binding.layout.collapseCockpit();
            binding.cockpit.setExpandCollapseButtonBackground(COLLAPSED);
        }
        cockpitCollapsed = !cockpitCollapsed;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_AND_MICROPHONE_PERMISSION_REQUEST) {
            for (int i = 0; i < permissions.length; i++) {
                switch (permissions[i]) {
                    case Manifest.permission.RECORD_AUDIO:
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            onCockpitMicrophoneButtonClicked();
                        }
                        break;
                    case Manifest.permission.CAMERA:
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            onCockpitCameraButtonClicked();
                        }
                        break;
                }
            }
        }
    }

    @Override
    public void onMeExpelled(Me me) {
        finish();
    }

    @Override
    public void onConferenceEnded(Conference conference) {
        finish();
    }

    private void alert(String message) {
        new AlertDialog.Builder(this).setMessage(message)
                                     .setNeutralButton(android.R.string.ok, (dialog, which) -> finish()).create()
                                     .show();
    }

    @Override
    public void finish() {
        exitConferenceAndCloseMyStream();
        super.finish();
    }

    private void exitConferenceAndCloseMyStream() {
        binding.conferenceVideo.setMediaStream(null);
        if (session != null) {
            MindSDK.exit2(session);
            session = null;
        }
        microphone.release();
        camera.release();
    }

}
