package com.mind.confy;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Vibrator;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.Intents.Scan;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DefaultDecoderFactory;
import com.journeyapps.barcodescanner.camera.CameraSettings;
import com.mind.api.sdk.ConferenceLayout;
import com.mind.confy.databinding.MainActivityBinding;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicReference;

public class MainActivity extends FullscreenActivity implements BarcodeCallback {

    private static final String TAG = "Confy";
    private static final String[] CONFERENCE_NAMES = { "Cassiopeia", "Andromeda", "Taurus", "Scorpius", "Auriga", "Orion", "Hydrus", "Centaurus", "Draco", "Lyra" };
    private static final int CAMERA_PERMISSION_REQUEST = new Random().nextInt(Short.MAX_VALUE); // (facepalm) Android allow us to use only 16 bits for `requestCode`...

    private MainActivityBinding binding;

    private final AtomicReference<String> roomURI = new AtomicReference<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity);
        binding.createRoomButton.setOnClickListener(view -> onCreateRoomButtonClicked());
    }

    @Override
    protected void onStart() {
        super.onStart();
        roomURI.set(null);
        CameraSettings cameraSettings = new CameraSettings();
        cameraSettings.setContinuousFocusEnabled(true);
        binding.qrCodeScanningView.setCameraSettings(cameraSettings);
        binding.qrCodeScanningView.setDecoderFactory(new DefaultDecoderFactory(Collections.singletonList(BarcodeFormat.QR_CODE), null, null, Scan.MIXED_SCAN));
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            onRequestPermissionsResult(CAMERA_PERMISSION_REQUEST, new String[]{ Manifest.permission.CAMERA }, new int[] { PackageManager.PERMISSION_GRANTED });
        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[] { Manifest.permission.CAMERA }, CAMERA_PERMISSION_REQUEST);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        binding.qrCodeScanningView.pause();
        binding.qrCodeScanningView.stopDecoding();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_PERMISSION_REQUEST) {
            for (int i = 0; i < permissions.length; i++) {
                if (Manifest.permission.CAMERA.equals(permissions[i]) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    binding.qrCodeScanningView.decodeContinuous(this);
                    binding.qrCodeScanningView.resume();
                }
            }
        }
    }

    public void onRoomURIDetected(String roomURI) {
        if (this.roomURI.compareAndSet(null, roomURI)) {
            Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            if (vibrator != null) {
                vibrator.vibrate(100);
            }
            startActivity(new Intent(this, RoomActivity.class).putExtra("ROOM_URI", roomURI));
        }
    }

    public void onCreateRoomButtonClicked() {
        if (this.roomURI.compareAndSet(null, "pending")) {
            MindAPI.createConference(CONFERENCE_NAMES[new Random().nextInt(CONFERENCE_NAMES.length)], ConferenceLayout.MOSAIC).thenAccept(conferenceURI -> {
                this.roomURI.set(conferenceURI.replace("/00e1db72-c57d-478c-88be-3533d43c8b34/", "/demo/#/"));
                startActivity(new Intent(this, RoomActivity.class).putExtra("ROOM_URI", roomURI.get()));
            });
        }
    }

    @Override
    public void barcodeResult(BarcodeResult result) {
        String text = result.getText();
        if (text != null) {
            if (text.matches("^https?://[^/]+.*/(#/|\\?)[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$")) {
                runOnUiThread(() -> onRoomURIDetected(text));
            }
        }
    }

    @Override
    public void possibleResultPoints(List<ResultPoint> resultPoints) {}

}
