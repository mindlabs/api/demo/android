package com.mind.confy;

import com.mind.api.sdk.MindSDK;
import com.mind.api.sdk.MindSDKOptions;

public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        MindSDKOptions options = new MindSDKOptions(this);
        MindSDK.initialize(options);
    }

}
